import functools
import json
import pathlib
import subprocess
import sys
import unittest.mock as mock

import pop.hub
import pytest

TPATH_DIR = pathlib.Path(__file__).parent.parent / "tpath"


@pytest.fixture(scope="function")
def hub():
    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.sub.add(dyne_name="tree")

    hub.pop.sub.add("tests.integration.mod", omit_class=False)

    with mock.patch("sys.exit"):
        yield hub


def cli(subcommand: str, *args, **kwargs):
    env = kwargs.pop("env", {})
    assert TPATH_DIR.exists()
    env["PYTHONPATH"] = TPATH_DIR
    kwargs["env"] = env
    cmd = [sys.executable, subcommand, *args]
    if not any("--output" in c for c in cmd):
        cmd.append("--output=json")

    proc = subprocess.check_output(cmd, **kwargs)
    stdout = proc.decode()

    try:
        return json.loads(stdout)
    except:
        return stdout


@pytest.fixture(scope="function", name="cli_doc")
def get_doc_cli():
    return functools.partial(
        cli, str(pathlib.Path(__file__).parent.parent.parent / "popdoc.py")
    )


@pytest.fixture(scope="function", name="cli_tree")
def get_tree_cli():
    return functools.partial(
        cli, pathlib.Path(__file__).parent.parent.parent / "run.py"
    )
